// Seleccionamos el formulario por su ID
const form = document.getElementById('superhero-form');

// Agregamos un "listener" para el evento "submit"
form.addEventListener('submit', function(event) {
  // Prevenimos que el formulario se envíe
  event.preventDefault();
  
  // Mostramos un alert cuando se envía el formulario
  alert('¡Formulario enviado!');
});
